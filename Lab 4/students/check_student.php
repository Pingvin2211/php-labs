<?php
// Отримуємо значення параметру name з GET-запиту
$name = $_GET['name'];

// Зчитуємо вміст файлу students.json
$studentsData = file_get_contents('students.json');

// Парсимо JSON у масив PHP
$students = json_decode($studentsData, true);

// Шукаємо студента з введеним і
foreach ($students as $student) {
if ($student['name'] === $name) {
// Якщо студент знайдений, повертаємо список його мов у форматі JSON
$languages = $student['languages'];
echo json_encode(array('result' => implode(', ', $languages)));
exit();
}
}

// Якщо студента не знайдено, повертаємо рядок "NotFound"
echo json_encode(array('result' => 'NotFound'));
?>