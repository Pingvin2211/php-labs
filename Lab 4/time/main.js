function showTime() {
    var date = new Date(); // створюємо об'єкт Date
    var year = date.getFullYear(); // отримуємо рік
    var month = date.getMonth() + 1; // отримуємо місяць (нумерація місяців починається з 0)
    var day = date.getDate(); // отримуємо число
    var hours = date.getHours(); // отримуємо години
    var minutes = date.getMinutes(); // отримуємо хвилини
    var seconds = date.getSeconds(); // отримуємо секунди
    var ampm = hours >= 12 ? 'PM' : 'AM'; // перевіряємо, щоб відображати AM або PM
    hours = hours % 12; // конвертуємо години у 12-годинний формат
    hours = hours ? hours : 12; // якщо години дорівнюють 0, то відображаємо 12
    minutes = minutes < 10 ? '0'+minutes : minutes; // додаємо 0 до хвилин, якщо вони менші за 10
    seconds = seconds < 10 ? '0'+seconds : seconds; // додаємо 0 до секунд, якщо вони менші за 10
    var time = hours + ':' + minutes + ':' + seconds + ' ' + ampm; // формуємо рядок з часом
    var dateStr = month + '/' + day + '/' + year; // формуємо рядок з датою
    document.getElementById('time').innerHTML = time + ' ' + dateStr; // виводимо час і дату на сторінку
}