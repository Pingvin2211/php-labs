<?php
if(isset($_GET['city'])) {
  $city = $_GET['city'];
  $apiKey = '36c346c3debc05dc4d27c7239653a7bd';
  $url = 'http://api.openweathermap.org/data/2.5/weather?q=' . urlencode($city) . '&appid=' . $apiKey . '&units=metric';
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  $data = curl_exec($ch);
  curl_close($ch);
  $weatherData = json_decode($data);
  if($weatherData->cod == 200) {
    $html = '<h2>Current Weather in ' . $city . '</h2>';
    $html .= '<p>Temperature: ' . $weatherData->main->temp . ' &deg;C</p>';
    $html .= '<p>Humidity: ' . $weatherData->main->humidity . '%</p>';
    $html .= '<p>Wind Speed: ' . $weatherData->wind->speed . ' m/s</p>';
    echo $html;
  } else {
    echo '<p>Unable to get weather data for ' . $city . '</p>';
  }
}
?>