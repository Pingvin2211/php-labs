function getWeather() {
    // отримуємо координати з форми
    var lat = document.getElementById("lat").value;
    var lon = document.getElementById("lon").value;
  
    // формуємо URL для запиту на сервер OpenWeatherMap
    var url =
      "https://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&appid={36c346c3debc05dc4d27c7239653a7bd}&units=metric";
  
    // виконуємо запит на сервер OpenWeatherMap
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        // оброблюємо результат запиту
        var weatherData = JSON.parse(this.responseText);
        var temp = weatherData.main.temp;
        var description = weatherData.weather[0].description;
        var icon =
          "http://openweathermap.org/img/w/" +
          weatherData.weather[0].icon +
          ".png";
  var weatherBlock = document.getElementById("weather-block");
  var weatherHTML =
  "<img src='" +
       icon +
       "' alt='weather icon' width='50' height='50'>" +
  "<p>Temperature: " +
  temp +
  "°C</p>" +
  "<p>Description: " +
  description +
  "</p>";
  weatherBlock.innerHTML = weatherHTML;
  }
  };
  xhr.open("GET", url, true);
  xhr.send();
  }