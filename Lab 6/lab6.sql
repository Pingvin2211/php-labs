-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Дек 11 2023 г., 14:40
-- Версия сервера: 5.7.33
-- Версия PHP: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `lab6`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admins`
--

CREATE TABLE `admins` (
  `ACode` int(11) NOT NULL,
  `ALogin` varchar(50) NOT NULL,
  `APassword` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admins`
--

INSERT INTO `admins` (`ACode`, `ALogin`, `APassword`) VALUES
(1, 'admin', '202cb962ac59075b964b07152d234b70');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `UCode` int(11) NOT NULL,
  `UName` varchar(50) NOT NULL,
  `USurname` varchar(50) NOT NULL,
  `UEmail` varchar(50) NOT NULL,
  `UPassword` varchar(50) NOT NULL,
  `UPhone` varchar(50) DEFAULT NULL,
  `UAddress` varchar(100) DEFAULT NULL,
  `AvatarPath` varchar(255) DEFAULT 'noavatar.png',
  `UAvatar` varchar(255) DEFAULT 'noavatar.png',
  `avatar_path` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`UCode`, `UName`, `USurname`, `UEmail`, `UPassword`, `UPhone`, `UAddress`, `AvatarPath`, `UAvatar`, `avatar_path`) VALUES
(3, 'Alex', 'igor', 'igor.gayday.03@gmail.com', '28165601c7c1fbf23f7c06ad01dd5dc6', NULL, NULL, 'noavatar.png', 'noavatar.png', 'img/avatar_6459ff9a10d1e.png'),
(4, 'lol', 'igor', 'igor.gayday@gmail.com', '28165601c7c1fbf23f7c06ad01dd5dc6', NULL, NULL, 'noavatar.png', 'noavatar.png', 'img/avatar_6459ff8be694e.png'),
(5, 'Alex', 'igor', 'igor.gayda@gmail.com', '28165601c7c1fbf23f7c06ad01dd5dc6', NULL, NULL, 'noavatar.png', 'noavatar.png', 'img/avatar_645cd4086b010.png');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`ACode`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UCode`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admins`
--
ALTER TABLE `admins`
  MODIFY `ACode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `UCode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
