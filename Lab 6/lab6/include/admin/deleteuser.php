<?php

session_start();

require_once '../connect.php';

header('Content-Type: text/html; charset=utf-8');

$ucode = $_GET['id'];

mysqli_query($connect, "update `orders` set `orders`.`OCodeUser` = NULL where `orders`.`OCodeUser` = {$ucode}");

mysqli_query($connect, "delete from `users` where `users`.`UCode` = {$ucode}");

unset($_SESSION['user']);

header('Location: /admin/users');