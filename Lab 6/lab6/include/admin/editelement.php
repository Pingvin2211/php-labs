<?php

session_start();

require_once '../connect.php';

header('Content-Type: text/html; charset=utf-8');

$id = $_GET['id'];
$edit = $_GET['edit'];
$text = $_POST['text'];

switch ($edit) {
    case "country": { $sql_text = "update `countryproduct` set `countryproduct`.`CPName` = '{$text}' where `countryproduct`.`CPCode` = {$id}"; break;}
    case "name": { $sql_text = "update `nameproduct` set `nameproduct`.`NPName` = '{$text}' where `nameproduct`.`NPCode` = {$id}"; break;}
    case "brand": { $sql_text = "update `brandproduct` set `brandproduct`.`BPName` = '{$text}' where `brandproduct`.`BPCode` = {$id}"; break;}
}

mysqli_query($connect, $sql_text);

$_SESSION['message'] = 'Дані оновлено';
header('Location: /admin/edit'.$edit.'/'.$id);
exit();