<?php

session_start();

require_once '../connect.php';

header('Content-Type: text/html; charset=utf-8');

unset($_SESSION['admin']);
header('Location: /');