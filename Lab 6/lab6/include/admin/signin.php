<?php

session_start();

require_once '../connect.php';

header('Content-Type: text/html; charset=utf-8');

$login = $_POST['login'];
$password = $_POST['password'];

if (empty($login) || empty($password)) {
    $_SESSION['message']['admin_error'] = "Заповніть всі поля";
    if (!empty($login)) {
        $_SESSION['message']['admin_login'] = $login;
    }
    header('Location: /admin');
    exit();
} else {
    $password = md5($_POST['password']);
    $sql_admin = mysqli_query($connect, "select * from `admins` where `admins`.`ALogin` = '{$login}' and `admins`.`APassword` = '{$password}'");

    if (mysqli_num_rows($sql_admin) > 0) {
        $admin = mysqli_fetch_assoc($sql_admin);
        $_SESSION['admin'] = [
            "ACode" => $admin['ACode']
        ];
        unset($_SESSION['message']);
        unset($password);
        header('Location: /admin/administration');
        exit();
    } else {
        $_SESSION['message']['admin_error'] = "Не правильний логін або пароль";
        header('Location: /admin');
        exit();
    }
}