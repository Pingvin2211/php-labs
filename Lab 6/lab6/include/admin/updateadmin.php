<?php

session_start();

require_once '../connect.php';

header('Content-Type: text/html; charset=utf-8');

$login = $_POST['login'];
$oldpassword = $_POST['oldpassword'];
$newpassword = $_POST['newpassword'];
$verificationpassword = $_POST['verificationpassword'];

if (empty($login)) {
    $_SESSION['message']['admin_error'] = "Заповніть виділені поля";
    $_SESSION['message']['admin_error_login'] = true;
    header('Location: /admin/administration');
    exit();
} else {
    $acode = $_SESSION['admin']['ACode'];
    mysqli_query($connect, "update `admins` set `admins`.`ALogin` = '{$login}' where `admins`.`ACode` = {$acode}");

    if (!empty($oldpassword) || !empty($newpassword) || !empty($verificationpassword)) {
        if (empty($oldpassword) || empty($newpassword) || empty($verificationpassword)) {
            $_SESSION['message']['admin_error'] = "Для зміни пароля заповніть всі поля";
            header('Location: /admin/administration');
            exit();
        } else {
            $verificationpasswordadmin = md5($oldpassword);
            $sql_oldpassword_admin = mysqli_query($connect, "select * from `admins` where `admins`.`ACode` = {$acode} and `admins`.`APassword` = '{$verificationpasswordadmin}'");
            if (mysqli_num_rows($sql_oldpassword_admin) > 0) {
                if ($newpassword != $verificationpassword) {
                    $_SESSION['message']['admin_error'] = "Різні паролі";
                    header('Location: /admin/administration');
                    exit();
                }else {
                    if ($oldpassword == $newpassword) {
                        $_SESSION['message']['admin_error'] = "Ви ввели існуючий пароль";
                        header('Location: /admin/administration');
                        exit();
                    } else {
                        $newpassword = md5($newpassword);
                        mysqli_query($connect, "update `admins` set `admins`.`APassword` = '{$newpassword}' where `admins`.`ACode` = {$acode}");
                        unset($verificationpassworduser);
                        unset($newpassword);
                        unset($oldpassword);
                        unset($_SESSION['message']);
                        header('Location: /admin/administration');
                        exit();
                    }
                }
            } else {
                $_SESSION['message']['admin_error'] = "Пароль не вірний";
                header('Location: /admin/administration');
                exit();
            }
        }
    }

    header('Location: /admin/administration');
    exit();
}