<?php

session_start();

require_once 'connect.php';

header('Content-Type: text/html; charset=utf-8');

$oldpassword = $_POST['oldpassword'];
$newpassword = $_POST['newpassword'];
$verificationpassword = $_POST['verificationpassword'];

if (empty($oldpassword) || empty($newpassword) || empty($verificationpassword)) {
    $_SESSION['message']['passwordchanger-error'] = "Заповніть всі поля";
    header('Location: /passwordchanger');
    exit();
} else {
    $ucode = $_SESSION['user']['UCode'];
    $verificationpassworduser = md5($oldpassword);
    $sql_oldpassword_user = mysqli_query($connect, "select `users`.`UPassword` from `users` where `users`.`UCode` = {$ucode} and `users`.`UPassword` = '{$verificationpassworduser}'");
    if (mysqli_num_rows($sql_oldpassword_user) > 0) {
        if ($newpassword != $verificationpassword) {
            $_SESSION['message']['passwordchanger-error'] = "Різні паролі";
            header('Location: /passwordchanger');
            exit();
        } else {
            if ($oldpassword == $newpassword) {
                $_SESSION['message']['passwordchanger-error'] = "Ви ввели існуючий пароль";
                header('Location: /passwordchanger');
                exit();
            } else {
                $newpassword = md5($newpassword);
                mysqli_query($connect, "update `users` set `users`.`UPassword` = '{$newpassword}' where `users`.`UCode` = {$ucode}");
                unset($verificationpassworduser);
                unset($newpassword);
                unset($oldpassword);
                unset($_SESSION['message']);
                header('Location: /account');
                exit();
            }
        }
    } else {
        $_SESSION['message']['passwordchanger-error'] = "Пароль не вірний";
        header('Location: /passwordchanger');
        exit();
    }
}