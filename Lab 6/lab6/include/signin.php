<?php

session_start();

require_once 'connect.php';

header('Content-Type: text/html; charset=utf-8');

$email = $_POST['email'];
$password = $_POST['password'];

$_SESSION['message']['text_email'] = $email;

if (empty($email) || empty($password)) {
    $_SESSION['message']['signin-error'] = "Заповніть всі поля";
    if (empty($email)) {
        $_SESSION['message']['email'] = true;
    }
    if (empty($password)) {
        $_SESSION['message']['password'] = true;
    }
    header('Location: /signin');
    exit();
} else {
    $password = md5($_POST['password']);
    $sql_user = mysqli_query($connect, "select * from `users` where `users`.`UEmail` = '{$email}' and `users`.`UPassword` = '{$password}'");

    if (mysqli_num_rows($sql_user) > 0) {
        $user = mysqli_fetch_assoc($sql_user);

        $_SESSION['user'] = [
            "UCode" => $user['UCode']
        ];

        unset($_SESSION['message']);
        unset($password);
        header('Location: /');
        exit();
    } else {
        $_SESSION['message']['signin-error'] = 'Не правильний логін або пароль';
        header('Location: /signin');
        exit();
    }
}