<?php

session_start();

require_once 'connect.php';

header('Content-Type: text/html; charset=utf-8');

$name = $_POST['name'];
$surname = $_POST['surname'];
$email = $_POST['email'];
$password = $_POST['password'];
$verificationpassword = $_POST['verificationpassword'];

$_SESSION['message']['text_name'] = $name;
$_SESSION['message']['text_surname'] = $surname;
$_SESSION['message']['text_email'] = $email;

if (empty($name) || empty($surname) || empty($email) || empty($password) || empty($verificationpassword)) {
    $_SESSION['message']['signup-error'] = "Заповніть всі поля";
    if (empty($name)) {
        $_SESSION['message']['name'] = true;
    }
    if (empty($surname)) {
        $_SESSION['message']['surname'] = true;
    }
    if (empty($email)) {
        $_SESSION['message']['email'] = true;
    }
    if (empty($password)) {
        $_SESSION['message']['password'] = true;
    }
    if (empty($verificationpassword)) {
        $_SESSION['message']['verificationpassword'] = true;
    }
    header('Location: /signup');
    exit();
} else {
    if ($password === $verificationpassword) {
        $sql_email = mysqli_query($connect, "select * from `users` where `users`.`UEmail` = '{$email}'");
        if (mysqli_num_rows($sql_email) > 0) {
            $_SESSION['message']['signup-error'] = 'Пошта зарезервована';
            header('Location: /signup');
            exit();
        } else {
            $password = md5($password);
            mysqli_query($connect, "insert into `users` (`UName`, `USurname`, `UEmail`, `UPassword`) values ('{$name}', '{$surname}', '{$email}', '{$password}')");
            $ucode = mysqli_insert_id($connect);
            $_SESSION['user'] = [
                "UCode" => $ucode
            ];

            unset($_SESSION['message']);
            header('Location: /');
            exit();
        }
    } else {
        $_SESSION['message']['signup-error'] = 'Паролі різні';
        header('Location: /signup');
        exit();
    }
}