<?php

session_start();

require_once 'connect.php';

header('Content-Type: text/html; charset=utf-8');

$name = $_POST['name'];
$surname = $_POST['surname'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$address = $_POST['address'];

$_SESSION['message']['text_name'] = $name;
$_SESSION['message']['text_surname'] = $surname;
$_SESSION['message']['text_email'] = $email;
$_SESSION['message']['text_phone'] = $phone;
$_SESSION['message']['text_address'] = $address;

if (empty($name) || empty($surname) || empty($email)) {
    $_SESSION['message']['identity-error'] = "Заповніть всі поля";
    if (empty($name)) {
        $_SESSION['message']['name'] = true;
    }
    if (empty($surname)) {
        $_SESSION['message']['surname'] = true;
    }
    if (empty($email)) {
        $_SESSION['message']['email'] = true;
    }
    header('Location: /identity');
    exit();
} else {
    $ucode = $_SESSION['user']['UCode'];
    if (!empty($phone)) {
        $phone = ", `users`.`UPhone` = '".$phone."'";
    } else {
        $phone = ", `users`.`UPhone` = NULL";
    }
    if (!empty($address)) {
        $address = ", `users`.`UAddress` = '".$address."'";
    } else {
        $address = ", `users`.`UAddress` = NULL";
    }
    $sql = "update `users` set `users`.`UName` = '{$name}', `users`.`USurname` = '{$surname}', `users`.`UEmail` = '{$email}'{$phone}{$address} where `users`.`UCode` = {$ucode}";
    mysqli_query($connect, $sql);
    unset($_SESSION['message']);
    header('Location: /account');
    exit();
}