<?php

    session_start();

    require_once 'include/connect.php';
    if (isset($_GET['page'])) {
        $pages = array("admin","home", "signin", "signup", "account", "identity", "passwordchanger");
        if (in_array($_GET['page'], $pages)) {
            $page = $_GET['page'];
        } else {
            $page = "home";
        }
    } else {
        $page = "home";
    }
    if (!isset($_SESSION['user'])) {
        if ($page == 'account' || $page == 'identity' || $page == 'passwordchanger') {
            $page = 'signin';
        }
    }
?>

<!doctype html>
<html lang="ua">
<?php
    require_once 'page/element/head.php'
?>
<body>
    <div class="wrapper">

        <?php
            if ($page != "admin")  {
                require_once 'page/element/header.php';
            }

            require_once ("page/".$page.".php");
        ?>
    </div>
    <script>
        $(function() {
            $("#phone").mask("+380 (999) 99-99-99");
        });
    </script>
</body>
</html>