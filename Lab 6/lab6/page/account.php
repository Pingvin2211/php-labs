<main class="bc">
    <div class="container">
        <div class="account">
            <div class="account__row">
                <div class="account__side">
                    <div class="account__side-title">Мій кабінет</div>
                    <ul class="account__side-ul">
                        <li><a class="account__side-ul-active" href="/account">особисті дані</a></li>
                    </ul>
                </div>
                <div class="account__main">
                    <div class="account__main-title">особисті дані</div>
                    <div class="account__main-row">
                        <div class="account__main-inf">
                            <?
                            $ucode = $_SESSION['user']['UCode'];
                            $sql_user = mysqli_query($connect, "select * from `users` where `users`.`UCode` = {$ucode}");
                            $user = mysqli_fetch_assoc($sql_user);
                            ?>
                            <table>
                                <tbody>
                                    <tr>
                                        <td class="account__main-inf-title">Ім'я та прізвище</td>
                                        <td class="account__main-inf-text"><?=$user['UName'].' '.$user['USurname']?></td>
                                    </tr>
                                    <tr>
                                        <td class="account__main-inf-title">Електронна пошта</td>
                                        <td class="account__main-inf-text"><?=$user['UEmail']?></td>
                                    </tr>
                                    <tr>
                                        <td class="account__main-inf-title">Телефон</td>
                                        <td class="account__main-inf-text"><?if ($user['UPhone'] == '') echo "Не вказано"; else echo $user['UPhone'];?></td>
                                    </tr>
                                    <tr>
                                        <td class="account__main-inf-title">Аватарка</td>
                                        <td class="account__main-inf-text">
                                        <img src="<?= $user['avatar_path'] ?: 'img/noavatar.png' ?>" alt="" width="225" height="225">
                                        <form action="/upload_avatar.php" method="post" enctype="multipart/form-data">
                                                <input type="file" name="uploadfile">
                                                <input type="submit" name="upload" value="upload">
                                            </form>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="account__main-list">
                            <ul>
                                <li><a href="/identity">Редагувати особисті дані</a></li>
                                <li><a href="/passwordchanger">Змінити пароль</a></li>
                                <li><a href="/include/logout.php">Вихід</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
