<?php
if (isset($_SESSION['admin'])) {
    require_once 'page/element/adminheader.php';
    ?>
    <main class="bc">
        <div class="container">
            <div class="account">
                <div class="account__row">
                    <div class="account__side">
                        <div class="account__side-title">Адмін меню</div>
                        <?
                            if (isset($_GET['type_page'])) {
                                $type_page = $_GET['type_page'];
                                switch ($type_page) {
                                    case "administration": {$administration = true; break;}
                                    case "users": { $users = true; break;}
                                }
                            } else {
                                $administration = true;
                            }
                            ?>
                        <ul class="account__side-ul">
                            <li><a <?if ($administration) echo "class=\"account__side-ul-active\""?> href="/admin/administration">Адміністрування</a></li>
                            <li><a <?if ($users) echo "class=\"account__side-ul-active\""?> href="/admin/users">Користувачі</a></li>
                        </ul>
                    </div>
                    <?php
                    if (isset($_GET['type_page'])) {
                        $type_page = $_GET['type_page'];
                        switch ($type_page) {
                            case "administration": {require_once 'page/administration.php'; break;}
                            case "users": { require_once 'page/users.php';break;}
                        }
                    } else {
                        require_once 'page/administration.php';
                    }
                    ?>
                </div>
            </div>
        </div>
    </main>
<?
} else {
    ?>
    <main class="bc">
        <div class="admin-signin">
            <form class="admin-signin__form" action="/include/admin/signin.php" method="post">
                <div class="admin-signin__title">Вхід в адмін панель</div>
                <div class="admin-signin__item">
                    <div class="admin-signin__title-input"><label for="">Логін</label></div>
                    <div class="admin-signin__input"><input type="text" name="login" value="<?=$_SESSION['message']['admin_login']?>"></div>
                </div>
                <div class="admin-signin__item">
                    <div class="admin-signin__title-input"><label for="">Пароль</label></div>
                    <div class="admin-signin__input"><input type="password" name="password"></div>
                </div>
                <div class="admin-signin__message"><?=$_SESSION['message']['admin_error']?></div>
                <?unset($_SESSION['message'])?>
                <div class="admin-signin__button">
                    <button type="submit">Вхід</button>
                </div>
            </form>
        </div>
    </main>
    <?php
}
?>