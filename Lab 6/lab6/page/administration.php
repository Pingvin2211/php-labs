<div class="account__main">
    <div class="account__main-title">Дані адміністратора</div>
    <div class="admin-inf">
        <form action="/include/admin/updateadmin.php" class="admin-inf__form" method="post">
            <?
            $acode = $_SESSION['admin']['ACode'];
            $sql_admin = mysqli_query($connect, "select * from `admins` where `admins`.`ACode` = {$acode}");
            $admin = mysqli_fetch_assoc($sql_admin);
            ?>
            <div class="admin-inf__item">
                <div class="admin-inf__title"><label>Логін</label></div>
                <div class="admin-inf__input"><input <?if ($_SESSION['message']['admin_error_login']) echo "class=\"admin-inf__input-error\"";?> type="text" name="login" value="<?if (!$_SESSION['message']['admin_error_login']) echo $admin['ALogin'];?>"></div>
            </div>
            <div class="admin-inf__item">
                <div class="admin-inf__title"><label>Старий пароль</label></div>
                <div class="admin-inf__input"><input type="password" name="oldpassword"></div>
            </div>
            <div class="admin-inf__item">
                <div class="admin-inf__title"><label>Новий пароль</label></div>
                <div class="admin-inf__input"><input type="password" name="newpassword"></div>
            </div>
            <div class="admin-inf__item">
                <div class="admin-inf__title"><label>Підтвердіть новий пароль</label></div>
                <div class="admin-inf__input"><input type="password" name="verificationpassword"></div>
            </div>
            <div class="admin-signin__message"><?=$_SESSION['message']['admin_error']?></div>
            <div class="admin-inf__item">
                <div class="admin-inf__margin"></div>
                <div class="admin-inf__button"><button type="submit">Редагувати</button></div>
            </div>
        </form>
        <?unset($_SESSION['message'])?>
    </div>
</div>