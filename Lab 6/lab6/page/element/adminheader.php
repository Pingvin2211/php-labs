<header>
    <nav class="nav-top">
        <div class="container">
            <div class="nav-top__body">
                <div class="nav-top__inf">
                    <p>Адмін панель</p>
                </div>
                <div class="nav-top__user">
                    <a href="/include/admin/logout.php">
                        <p>Вихід</p>
                        <i class="fa fa-user-circle" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
    </nav>
</header>
