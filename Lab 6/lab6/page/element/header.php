<header>
    <nav class="nav-top">
        <div class="container">
            <div class="nav-top__body">
                
                <div class="nav-top__user">
                    <?if (isset($_SESSION['user'])) {
                        ?>
                        <a href="/account">
                            <p>Акаунт</p>
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                        </a>
                        <?
                    } else {
                        ?>
                        <a href="/signin">
                            <p>Вхід</p>
                            <i class="fa fa-user-circle" aria-hidden="true"></i>
                        </a>
                        <?
                    }?>
                </div>
            </div>
        </div>
    </nav>
    
</header>