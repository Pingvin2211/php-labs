
    <div class="benefits">
        <div class="container">
            <div class="benefits__text-center">найкращий онлайн-магазин спортивного харчування</div>
            <div class="benefits__row">
                <div class="benefit">
                    <div class="benefit__item">
                        <div class="benefit__title">
                            <div class="benefit__title-img"><img src="img/benefits/delivery.png" alt=""></div>
                            <div class="benefit__title-text">швидка<br>доставка</div>
                        </div>
                        <div class="benefit__text">
                            <ul class="benefit__ul">
                                <li>Новою поштою по Україні 1-2 дня</li>
                                <li>Кур'єром по Бердичеву - 40 грн</li>
                                <li>Самовивіз - Житомир, пров. Червоний 48</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="benefit">
                    <div class="benefit__item">
                        <div class="benefit__title">
                            <div class="benefit__title-img"><img src="img/benefits/guarantee.png" alt=""></div>
                            <div class="benefit__title-text">гарантія<br>якості</div>
                        </div>
                        <div class="benefit__text">
                            <ul class="benefit__ul">
                                <li>Актуальна наявність і ціна</li>
                                <li>100% оригінальний товар</li>
                                <li>Повернення здійснюється протягом 14 днів після отримання товару</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="benefit">
                    <div class="benefit__item">
                        <div class="benefit__title">
                            <div class="benefit__title-img"><img src="img/benefits/actual-nal.png" alt=""></div>
                            <div class="benefit__title-text">актуальна<br>наявність</div>
                        </div>
                        <div class="benefit__text">
                            <ul class="benefit__ul">
                                <li>Замовивши товар на сайті ви можете бути впевнені на 100%, що товар є в наявності і ціна не буде змінена.</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="benefit">
                    <div class="benefit__item">
                        <div class="benefit__title">
                            <div class="benefit__title-img"><img src="img/benefits/any-money.png" alt=""></div>
                            <div class="benefit__title-text">любий вид<br>оплати</div>
                        </div>
                        <div class="benefit__text">
                            <ul class="benefit__ul">
                                <li>Наявність в м. Житомир.</li>
                                <li>Накладений платіж.</li>
                                <li>Оплата онлайн (Visa / MasterCard).</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</main>