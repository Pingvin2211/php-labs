<main class="bc">
    <div class="container">
        <div class="identity">
            <div class="identity__row">
                <div class="identity__side">
                    <div class="identity__side-title">Мій кабінет</div>
                    <ul class="identity__side-ul">
                        <li><a class="identity__side-ul-active" href="/account">особисті дані</a></li>
                    </ul>
                </div>
                <div class="identity__main">
                    <div class="identity__main-title">редагуванн особистих даних</div>
                    <form action="/include/updateuser.php" class="identity__form" method="post">
                        <?
                        $ucode = $_SESSION['user']['UCode'];
                        $sql_user = mysqli_query($connect, "select * from `users` where `users`.`UCode` = {$ucode}");
                        $user = mysqli_fetch_assoc($sql_user);
                        ?>
                        <?
                        if ($_SESSION['message']['identity-error']) {
                            ?>
                            <div class="signin-error"><?=$_SESSION['message']['identity-error']?></div>
                            <?
                        }
                        ?>
                        <table>
                            <tbody>
                                <tr>
                                    <td class="identity__main-inf-title">Ім'я</td>
                                    <td class="identity__main-inf-input"><input <?if (isset($_SESSION['message']['name'])) echo "class=\"signin-form__input-error\"";?> type="text" name="name" value="<?if (isset($_SESSION['message']['text_name'])) echo $_SESSION['message']['text_name']; else echo $user['UName'];?>"></td>
                                </tr>
                                <tr>
                                    <td class="identity__main-inf-title">Прізвище</td>
                                    <td class="identity__main-inf-input"><input <?if (isset($_SESSION['message']['surname'])) echo "class=\"signin-form__input-error\"";?> type="text" name="surname" value="<?if (isset($_SESSION['message']['text_surname'])) echo $_SESSION['message']['text_surname']; else echo $user['USurname'];?>"></td>
                                </tr>
                                <tr>
                                    <td class="identity__main-inf-title">Електронна пошта</td>
                                    <td class="identity__main-inf-input"><input <?if (isset($_SESSION['message']['email'])) echo "class=\"signin-form__input-error\"";?> type="email" name="email" value="<?if (isset($_SESSION['message']['text_email'])) echo $_SESSION['message']['text_email']; else echo $user['UEmail'];?>"></td>
                                </tr>
                                <tr>
                                    <td class="identity__main-inf-title">Телефон</td>
                                    <td class="identity__main-inf-input"><input id="phone" type="text" name="phone" value="<?if (isset($_SESSION['message']['text_phone'])) echo $_SESSION['message']['text_phone']; else echo $user['UPhone'];?>" placeholder="+380 (___) __-__-__"></td>
                                </tr>
                                <tr>
                                    <td class="identity__main-inf-title-button"></td>
                                    <td colspan="2" class="identify__form-button">
                                        <button type="submit">зберегти</button>
                                        <a href="/account">відміна</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                    <?unset($_SESSION['message']);?>
                </div>
            </div>
        </div>
    </div>
</main>
