<main>
    <div class="container">
        <ul class="line-nav">
            <li><a href="/">Головна</a></li>
            <li>|</li>
            <li><span>Оформлення замовлення</span></li>
        </ul>
        <div class="order-title">оформлення замовлення</div>
        <div class="order">
            <div class="order-data">
                <div class="order-data__box">
                    <div class="order-data__title">введення даних</div>
                    <?
                    if (isset($_SESSION['user'])) {
                        $ucode = $_SESSION['user']['UCode'];
                        $sql_user = mysqli_query($connect, "select * from `users` where `users`.`UCode` = {$ucode}");
                        $user = mysqli_fetch_assoc($sql_user);
                    }
                    ?>
                    <form action="/include/addorder.php" class="order-data__form" method="post">
                        <div class="order-data-name">
                            <div class="order-data-name__label"><label>Ім'я</label></div>
                            <div class="order-data-name__input"><input <?if ($_SESSION['message']['name']) echo "class=\"order-data-name__input-error\""?> type="text" name="name" value="<?if (isset($_SESSION['message']['text_name'])) echo $_SESSION['message']['text_name']; else echo $user['UName'];?>"></div>
                        </div>
                        <div class="order-data-surname">
                            <div class="order-data-name__label"><label>Прізвище</label></div>
                            <div class="order-data-name__input"><input <?if ($_SESSION['message']['surname']) echo "class=\"order-data-surname__input-error\""?> type="text" name="surname" value="<?if (isset($_SESSION['message']['text_surname'])) echo $_SESSION['message']['text_surname']; else echo $user['USurname'];?>"></div>
                        </div>
                        <div class="order-data-phone">
                            <div class="order-data-name__label"><label>Електронна пошта</label></div>
                            <div class="order-data-name__input"><input <?if ($_SESSION['message']['email']) echo "class=\"order-data-email__input-error\""?> type="email" name="email" value="<?if (isset($_SESSION['message']['text_email'])) echo $_SESSION['message']['text_email']; else echo $user['UEmail'];?>"></div>
                        </div>
                        <div class="order-data-email">
                            <div class="order-data-name__label"><label>Телефон</label></div>
                            <div class="order-data-name__input"><input <?if ($_SESSION['message']['phone']) echo "class=\"order-data-phone__input-error\""?> id="phone" type="text" name="phone" placeholder="+380 (___) __-__-__" value="<?if (isset($_SESSION['message']['text_phone'])) echo $_SESSION['message']['text_phone']; else echo $user['UPhone'];?>"></div>
                        </div>
                        <div class="order-data-address">
                            <div class="order-data-name__label"><label>Адреса для доставки</label></div>
                            <div class="order-data-name__input"><input <?if ($_SESSION['message']['address']) echo "class=\"order-data-address__input-error\""?> type="text" name="address" value="<?if (isset($_SESSION['message']['text_address'])) echo $_SESSION['message']['text_address']; else echo $user['UAddress'];?>"></div>
                        </div>
                        <div class="order-data-submit">
                            <button type="submit">замовити</button>
                        </div>
                        <div class="order-data__form-error"><?if (isset($_SESSION['message']['form-error'])) echo  $_SESSION['message']['form-error'];?></div>
                        <?unset($_SESSION['message'])?>
                    </form>
                </div>
            </div>
            <div class="order-cart">
                <div class="order-cart__box">
                    <div class="order-cart__title">ваше замовлення</div>
                    <?
                    $sql_basket_product_text = "select * from `product` inner join `nameproduct` on `nameproduct`.`NPCode` = `product`.`PCodeName` inner join `typeproduct` on `typeproduct`.`TPCode` = `product`.`PCodeType` inner join `countryproduct` on `countryproduct`.`CPCode` = `product`.`PCodeCountry` inner join `brandproduct` on `brandproduct`.`BPCode` = `product`.`PCodeBrand` where `product`.`PCode` in (";
                    foreach ($_SESSION['basket'] as $key => $value) {
                        $sql_basket_product_text .= $key.",";
                    }
                    $sql_basket_product_text = substr($sql_basket_product_text, 0, -1).")";

                    $sql_basket_product = mysqli_query($connect, $sql_basket_product_text);
                    $basket_product = mysqli_fetch_all($sql_basket_product);

                    foreach ($basket_product as $i) {
                    ?>
                    <div class="order-cart__item">
                        <div class="order-cart__img"><img src="/<?=$i[7]?>" alt=""></div>
                        <div class="order-cart__content">
                            <div class="order-cart__content-title">
                                <a href="/product/<?=$i[0]?>"><?=$i[13]?></a>
                                <span><?=$i[8]?> г</span>
                            </div>
                            <div class="order-cart__content-detail">
                                <div class="order-cart__content-count"><?=$_SESSION['basket'][$i[0]]['quantity']?> шт.</div>
                                <div class="order-cart__content-price"><?=$_SESSION['basket'][$i[0]]['quantity']*$i[9]?> грн</div>
                            </div>
                        </div>
                    </div>
                    <?}?>
                    <div class="order-cart__total">
                        <div class="order-cart__total-text">Всього</div>
                        <div class="order-cart__total-amount"><?=$_SESSION['totalprice']?> грн</div>
                    </div>
                    <div class="order-cart__edit"><a href="/basket">Редагувати замовлення</a></div>
                </div>
            </div>
        </div>
    </div>
</main>