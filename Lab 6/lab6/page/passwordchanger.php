<main class="bc">
    <div class="container">
        <div class="identity">
            <div class="identity__row">
                <div class="identity__side">
                    <div class="identity__side-title">Мій кабінет</div>
                    <ul class="identity__side-ul">
                        <li><a class="identity__side-ul-active" href="/account">особисті дані</a></li>
                    </ul>
                </div>
                <div class="identity__main">
                    <div class="identity__main-title">редагуванн особистих даних</div>
                    <form action="/include/passwordchanger.php" class="identity__form" method="post">
                        <?
                        if ($_SESSION['message']['passwordchanger-error']) {
                            ?>
                            <div class="signin-error"><?=$_SESSION['message']['passwordchanger-error']?></div>
                            <?
                        }
                        ?>
                        <table>
                            <tbody>
                            <tr>
                                <td class="identity__main-inf-title">Старий пароль</td>
                                <td class="identity__main-inf-input"><input type="password" name="oldpassword"></td>
                            </tr>
                            <tr>
                                <td class="identity__main-inf-title">Новий пароль</td>
                                <td class="identity__main-inf-input"><input type="password" name="newpassword"></td>
                            </tr>
                            <tr>
                                <td class="identity__main-inf-title">Підствердіть новий пароль</td>
                                <td class="identity__main-inf-input"><input type="password" name="verificationpassword"></td>
                            </tr>
                            <tr>
                                <td class="identity__main-inf-title-button"></td>
                                <td colspan="2" class="identify__form-button">
                                    <button type="submit">зберегти</button>
                                    <a href="/account">відміна</a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                    <?unset($_SESSION['message']);?>
                </div>
            </div>
        </div>
    </div>
</main>
