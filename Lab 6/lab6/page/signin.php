<main>
    <div class="container">
        <ul class="line-nav">
            <li><a href="/">Головна</a></li>
            <li>|</li>
            <li><span>Авторизація</span></li>
        </ul>
        <div class="signin">
            <form action="/include/signin.php" class="signin-form" method="post">
                <div class="signin-form__title">Вхід в інтернет-магазин</div>
                <div class="signin-form__email">
                    <label for="email">Електронна пошта</label>
                    <input <?if (isset($_SESSION['message']['email'])) echo "class=\"signin-form__input-error\"";?> id="email" type="email" name="email" value="<?=$_SESSION['message']['text_email']?>">
                </div>
                <div class="signin-form__password">
                    <label for="password">Пароль</label>
                    <input <?if (isset($_SESSION['message']['password'])) echo "class=\"signin-form__input-error\"";?> id="password" type="password" name="password">
                </div>
                <div class="signin-form__signup"><a href="/signup">Реєстрація</a></div>
                <?
                if ($_SESSION['message']['signin-error']) {
                    ?>
                    <div class="signin-error"><?=$_SESSION['message']['signin-error']?></div>
                    <?
                }
                unset($_SESSION['message']);
                ?>
                <button type="submit">вхід</button>
            </form>
        </div>
    </div>
</main>