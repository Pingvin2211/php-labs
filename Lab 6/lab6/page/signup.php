<main>
    <div class="container">
        <ul class="line-nav">
            <li><a href="/">Головна</a></li>
            <li>|</li>
            <li><span>Реєстрація</span></li>
        </ul>
        <div class="signup">
            <form action="/include/signup.php" class="signup-form" method="post">
                <div class="signup-form__title">Реєстрація в інтернет-магазині</div>
                <div class="signup-form__surname">
                    <label for="surname">Прізвище</label>
                    <input <?if (isset($_SESSION['message']['name'])) echo "class=\"signun-form__input-error\"";?> id="surname" type="text" name="surname" value="<?=$_SESSION['message']['text_name']?>">
                </div>
                <div class="signup-form__name">
                    <label for="name">Ім'я</label>
                    <input <?if (isset($_SESSION['message']['surname'])) echo "class=\"signun-form__input-error\"";?> id="name" type="text" name="name" value="<?=$_SESSION['message']['text_surname']?>">
                </div>
                <div class="signup-form__email">
                    <label for="email">Електронна пошта</label>
                    <input <?if (isset($_SESSION['message']['email'])) echo "class=\"signun-form__input-error\"";?> id="email" type="email" name="email" value="<?=$_SESSION['message']['text_email']?>">
                </div>
                <div class="signup-form__password">
                    <label for="password">Пароль</label>
                    <input <?if (isset($_SESSION['message']['password'])) echo "class=\"signun-form__input-error\"";?> id="password" type="password" name="password">
                </div>
                <div class="signup-form__verification-password">
                    <label for="verificationpassword">Підтвердіть пароль</label>
                    <input <?if (isset($_SESSION['message']['verificationpassword'])) echo "class=\"signun-form__input-error\"";?> id="verificationpassword" type="password" name="verificationpassword">
                </div>
                <div class="signup-form__signin"><a href="/signin">Вхід</a></div>
                <?
                if ($_SESSION['message']['signup-error']) {
                    ?>
                    <div class="signup-error"><?=$_SESSION['message']['signup-error']?></div>
                    <?
                }
                unset($_SESSION['message']);
                ?>
                <button type="submit">Реєстрація</button>
            </form>
        </div>
    </div>
</main>