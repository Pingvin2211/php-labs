<div class="account__main">
    <div class="account__main-title">Користувачі інтернет-магазину</div>
    <?if ($_GET['active'] == "delete") {
        ?>
        <div class="order-history-none">Видалити користувача #<?=$_GET['id']?></div>
        <div class="choice">
            <a href="/include/admin/deleteuser.php?id=<?=$_GET['id']?>">Так</a>
            <a href="/admin/users">Ні</a>
        </div>
        <?
    } else {?>
    <?
    if (isset($_POST['search'])) {
        $search = "where `users`.`UCode` like '%".$_POST['search']."%'";
    } else {
        $search = "";
    }
    $sql_users = mysqli_query($connect, "select * from `users` {$search} order by `users`.`UCode` desc");

    if (mysqli_num_rows($sql_users) > 0) {
        echo "<div class=\"order-history-none\">Користувачі</div>";
        $users = mysqli_fetch_all($sql_users);
        ?>
        <div class="orders">
            <?foreach ($users as $i) {?>
                <div class="orders__item">
                    <div class="orders__item-title">Користувач #<?=$i[0]?></div>
                    <div class="orders__item-row">
                        <div class="orders__item-inf-customer">
                            <div class="orders__item-inf-customer-input">Введені дані</div>
                            <div class="orders__item-inf-customer-input">
                                <div class="orders__item-inf-customer-input-title">Ім'я та прізвище</div>
                                <div class="orders__item-inf-customer-input-value"><?=$i[1]?> <?=$i[2]?></div>
                            </div>
                            <div class="orders__item-inf-customer-input">
                                <div class="orders__item-inf-customer-input-title">Пошта</div>
                                <div class="orders__item-inf-customer-input-value"><?=$i[3]?></div>
                            </div>
                            <div class="orders__item-inf-customer-input">
                                <div class="orders__item-inf-customer-input-title">Телефон</div>
                                <div class="orders__item-inf-customer-input-value"><?if ($i[5] == '') echo "Не вказано"; else echo $i[5];?></div>
                            </div>
                            <div class="orders__item-inf-customer-input">
                                <div class="orders__item-inf-customer-input-title">Адреса</div>
                                <div class="orders__item-inf-customer-input-value"><?if ($i[6] == '') echo "Не вказано"; else echo $i[6];?></div>
                            </div>
                        </div>
                        <div class="orders__item-inf">
                            <div class="orders__item-inf-delete"><a href="/admin/users/delete/<?=$i[0]?>">Видалити</a></div>
                        </div>
                    </div>
                </div>
            <?}?>
        </div>
        <?
    } else {
        echo "<div class=\"order-history-none\">Користувачів не знайдено</div>";
    }}
    ?>
</div>