<?php
session_start();


$servername = "localhost";
$username = "root";
$password = "";
$dbname = "lab6";

// Создаем соединение с базой данных
$conn = new mysqli($servername, $username, $password, $dbname);
// Проверяем соединение
if ($conn->connect_error) {
  die("Connection failed: " . $conn->connect_error);
}

if ($_FILES["uploadfile"]["error"] == UPLOAD_ERR_OK) {
  // Проверяем, является ли файл изображением
  $check = getimagesize($_FILES["uploadfile"]["tmp_name"]);
  if ($check !== false) {
    // Генерируем уникальное имя файла
    $filename = "avatar_" . uniqid() . "." . pathinfo($_FILES["uploadfile"]["name"], PATHINFO_EXTENSION);
    // Сохраняем файл на сервере
    move_uploaded_file($_FILES["uploadfile"]["tmp_name"], "img/" . $filename);
    // Обновляем базу данных
    $sql = "UPDATE users SET avatar_path='img/$filename' WHERE UCode={$_SESSION['user']['UCode']}";
    if ($conn->query($sql) === TRUE) {
      echo "Avatar uploaded successfully.";
    } else {
      echo "Error updating record: " . $conn->error;
    }
  } else {
    echo "File is not an image.";
  }
} else {
  echo "Error uploading file.";
}
header('Location: /account');
$conn->close();
?>